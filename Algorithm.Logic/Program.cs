﻿
namespace Algorithm.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Program
    {
        /// <summary>
        /// PROBLEMA:
        /// 
        /// Implementar um algoritmo para o controle de posição de um drone em um plano cartesiano (X, Y).
        /// 
        /// O ponto inicial do drone é "(0, 0)" para cada execução do método Evaluate ao ser executado cada teste unitário.
        /// 
        /// A string de entrada pode conter os seguintes caracteres N, S, L, e O representando Norte, Sul, Leste e Oeste respectivamente.
        /// Estes catacteres podem estar presentes aleatóriamente na string de entrada.
        /// Uma string de entrada "NNNLLL" irá resultar em uma posição final "(3, 3)", assim como uma string "NLNLNL" irá resultar em "(3, 3)".
        /// 
        /// Caso o caracter X esteja presente, o mesmo irá cancelar a operação anterior. 
        /// Caso houver mais de um caracter X consecutivo, o mesmo cancelará mais de uma ação na quantidade em que o X estiver presente.
        /// Uma string de entrada "NNNXLLLXX" irá resultar em uma posição final "(1, 2)" pois a string poderia ser simplificada para "NNL".
        /// 
        /// Além disso, um número pode estar presente após o caracter da operação, representando o "passo" que a operação deve acumular.
		/// Este número deve estar compreendido entre 1 e 2147483647.
		/// Deve-se observar que a operação 'X' não suporta opção de "passo" e deve ser considerado inválido. Uma string de entrada "NNX2" deve ser considerada inválida.
        /// Uma string de entrada "N123LSX" irá resultar em uma posição final "(1, 123)" pois a string pode ser simplificada para "N123L"
        /// Uma string de entrada "NLS3X" irá resultar em uma posição final "(1, 1)" pois a string pode ser siplificada para "NL".
        /// 
        /// Caso a string de entrada seja inválida ou tenha algum outro problema, o resultado deve ser "(999, 999)".
        /// 
        /// OBSERVAÇÕES:
        /// Realizar uma implementação com padrões de código para ambiente de "produção". 
        /// Comentar o código explicando o que for relevânte para a solução do problema.
        /// Adicionar testes unitários para alcançar uma cobertura de testes relevânte.
        /// </summary>
        /// <param name="input">String no padrão "N1N2S3S4L5L6O7O8X"</param>
        /// <returns>String representando o ponto cartesiano após a execução dos comandos (X, Y)</returns>
        public static string Evaluate(string input)
        {
            int x = default(int), y = default(int);

            try
            {
                //Trata caracteres minúsculos
                TreatsLowerCase(ref input);

                // Valida inputs errados
                ValidateInput(input);

                var steps = input.ToCharArray();

                // Remove passos cancelados por X
                steps = RemoveCanceledSteps(steps);

                for (int i = 0; i < steps.Length; i++)
                {
                    // Varre todos os passos adicionando os passos acumulado, caso existam
                    switch (steps[i])
                    {
                        case 'N':
                            y += AccumulatedSteps(steps, i);
                            break;

                        case 'S':
                            y -= AccumulatedSteps(steps, i);
                            break;

                        case 'L':
                            x += AccumulatedSteps(steps, i);
                            break;

                        case 'O':
                            x -= AccumulatedSteps(steps, i);
                            break;

                        default:
                            break;
                    }
                }
            }
            catch (Exception)
            {
                return "(999, 999)";
            }

            return string.Format("({0}, {1})", x, y);
        }

        /// <summary>
        /// Valida entradas inválidas.
        /// </summary>
        /// <param name="input" >Comando com os passos a serem executados </param>
        private static void ValidateInput(string input)
        {
            // Valida se a entrada inicia com um passo N-S-L-O
            if (
                string.IsNullOrEmpty(input.Trim()) ||
                !char.IsLetter(input[0]) ||
                input[0] == 'X'
            )
            {
                throw new Exception();
            }

            bool lastWasX = true;

            foreach (var item in input)
            {
                // Verifica se há um dígito após um X
                if (lastWasX && char.IsDigit(item))
                    throw new Exception();

                lastWasX = item == 'X';

                // Valida permitindo apenas passos permitidos
                if (item != 'X' &&
                    item != 'N' &&
                    item != 'S' &&
                    item != 'L' &&
                    item != 'O' &&
                    !char.IsDigit(item))
                {
                    throw new Exception();
                }
            }
        }

        /// <summary>
        /// Retorna o número de passos acumulados, ou seja, a instrução em dígitos após o passo N-S-L-O se existir.
        /// </summary>
        /// <param name="steps">Passos a serem executados.</param>
        /// <param name="count">Contador da posição atual do passo a ser acumulado.</param>
        /// <returns></returns>
        private static int AccumulatedSteps(char[] steps, int count)
        {
            if (count < steps.Length - 1 && char.IsDigit(steps[count + 1]))
            {
                string accumulated = default(string);

                // Varre o número de repetições após o caractere de passo.
                for (int i = count + 1; i < steps.Length; i++)
                {
                    // Se o próximo caractere não for um dígito para de somar
                    if (!char.IsDigit(steps[i]))
                        break;

                    accumulated += steps[i];
                }

                int result = Convert.ToInt32(accumulated);

                // Causa exceção se for maior ou igual o tamanho máximo do tipo inteiro, pois foi definido em regra como estouro de pilha.
                if (result >= int.MaxValue)
                    throw new Exception();

                return result;
            }

            return 1;
        }

        /// <summary>
        /// Remove da instrução os passos cancelados por X.
        /// </summary>
        /// <param name="steps">Passos a serem limpos.</param>
        /// <returns></returns>
        private static char[] RemoveCanceledSteps(char[] steps)
        {
            // Lista que retornará os passos limpos.
            List<char> stepsToReturn = new List<char>();

            int stepsToRemove = 0;

            // Varre os passos de trás para frente, pois o X sempre vem logo após o passo a ser cancelado.
            for (int i = steps.Length - 1; i >= 0; i--)
            {
                if (steps[i] == 'X')
                {
                    // Pode incrementar os passos a serem removidos de acordo com a quantidade de X encontrados.
                    stepsToRemove++;
                }
                else
                {
                    if (stepsToRemove > 0) // Se há passos a serem removidos
                    {
                        if (!char.IsDigit(steps[i]))
                            stepsToRemove--; // Decrementa por ter ignorado a adição de um passo
                    }
                    else
                    {
                        stepsToReturn.Add(steps[i]); // Adiciona um passo ao retorno
                    }
                }
            }

            // Inverte, pois foram adicionados de trás pra frente devido ao for invertido.
            stepsToReturn.Reverse();

            return stepsToReturn.ToArray();
        }

        /// <summary>
        /// Transforma a entrada em Upper Case para lidar com os comandos
        /// </summary>
        /// <param name="input">Comando com os passos a serem executados</param>
        private static void TreatsLowerCase(ref string input)
        {
            input = input.ToUpper();
        }
    }
}
